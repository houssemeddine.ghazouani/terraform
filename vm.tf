//--------------------------------->section vAPP VM1<----------------------------------------------------------//
locals {
  vm=csvdecode(file("./ressource/Classeur1.csv"))
 }

  output "vm" {
    value = local.vm
  }

resource "vcd_vapp_vm" "testVM1" {
  for_each= {for vm in local.vm :  vm.VMName=> vm}

  org           = "Workshop"
  vdc           = "Workshop-VDC"
  cpus          = each.value.CPU
  memory        = each.value.RAM
  computer_name = each.value.HostName
  name          = each.value.VMName
  catalog_name  = "vm-templates"
  template_name = each.value.Template
  vapp_name     = vcd_vapp.testvapp.name

  /*network {
  name = "net-windows"
  type = "org"
  ip_allocation_mode ="MANUAL"
  ip=each.value.ip
  }*/
}
  /*disk {
    name        = each.value.diskname
    bus_number  = 1
    unit_number = 0
  }
  /*disk {
    name        = vcd_independent_disk.myNewIndependentDisk2.name
    bus_number  = 2
    unit_number = 0
  }*/
  /*depends_on = [vcd_independent_disk.myNewIndependentDisk1] /*,vcd_network_routed.network]*/

  /*provisioner "file" {
  source = "script.yml"
  destination = "/script.yml"
    connection {
    host = ""
    password = "ftsFTS123!"
    user = "root"
    type = "ssh"

  }
}
provisioner "file" {
  source = "script2.yml"
  destination = "/script2.yml"
    connection {
    host = ""
    password = "ftsFTS123!"
    user = "root"
    type = "ssh"
  }
}
provisioner "file" {
  source = "inventory"
  destination = "/inventory"
    connection {
    host = ""
    password = "ftsFTS123!"
    user = "root"
    type = "ssh"
  
}*/
  /*provisioner "file" {
  source = "disk1.csv"
  destination = "/disk1.csv"
    connection {
    host = ""
    password = "********"
    user = "root"
    type = "ssh"
  }
}
provisioner "file" {
  source = "disk2.csv"
  destination = "/disk2.csv"
    connection {
    host = ""
    password = "********"
    user = "root"
    type = "ssh"
  }
}*/
  /*provisioner "remote-exec" {
  inline = [//"chmod 777 /disk2.csv",//"chmod 777 /disk1.csv",
            "chmod 777 /script2.yml"," chmod 777 /script.yml ", 
            " chmod 777 /inventory ", "cd /" ,
            " export ANSIBLE_HOST_KEY_CHECKING=false",
            "ansible-playbook  -i inventory script.yml",
            "ansible-playbook  -i inventory script2.yml"]

   connection {
    host = ""
    password = "ftsFTS123!"
    user = "root"
    type = "ssh"
  }
}*/




/*--------------------------------------------------------------*/


/*resource "vcd_vm" "testVM2" {
  org           = "IBS"
  vdc           = "IBS-vdc"
  cpus          = 4
  memory        = 8192
  computer_name = "AD"
  name          = "VM-AD-IBS"
  catalog_name  = "Catalog-Prod"
  template_name = each.value.Template


  /*network {
  name = "net-windows"
  type = "org"
  ip_allocation_mode ="MANUAL"
  ip=each.value.ip
  }*/


